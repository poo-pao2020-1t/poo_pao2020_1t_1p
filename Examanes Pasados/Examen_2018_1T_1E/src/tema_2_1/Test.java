/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_2_1;

/**
 *
 * @author Usuario
 */


interface GFG {
    void myMethod();
    void getInfo();
}

abstract class Geeks implements GFG
{
    void getData(){
     System.out.println("GFG");
    }
}

public class Test extends Geeks
{
    public void myMethod(String message){
    System.out.println(message);
    }
    
    public void getInfo(){
    System.out.println("Geeks");
    }
    
    public static void main(String[] args){
    Geeks obj = new Test();
    obj.myMethod("GeeksforGeeks");
    }

    
}

