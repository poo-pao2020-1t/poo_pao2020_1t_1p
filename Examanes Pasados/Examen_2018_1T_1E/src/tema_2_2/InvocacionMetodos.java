/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_2_2;

/**
 *
 * @author Usuario
 */

class C {
    void p(C c) { //aqui es donde pasa la magia
     System.out.print("AB ");
    }
    
    void p(D d) {
     System.out.print("CD ");
    }
}

class D extends C {
    //p(D d) return "CD"
    
    @Override
    void p(C c) {   ///aqui es donde pasa la magia
     System.out.print("DE ");   //debio retornar AB pero retorna DE con la sobreescritura
    }
}
public class InvocacionMetodos {    
    public static void main(String[] args){
        C obj = new D();   //D() solo puede acceder a los metodos que estan en C
        obj.p(obj);
        ((D) obj).p(obj);
        obj.p((D) obj);
    }
}
