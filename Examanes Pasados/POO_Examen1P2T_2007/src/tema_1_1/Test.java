/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_1_1;

/**
 *
 * @author Usuario
 */

interface A{
 	public abstract void method1();
  }
class One implements A{
      public void method1(){
              System.out.println("hello");
      }
}
class Two extends One{
    public void dormir(){
        //System.out.println("dormir");
    }
}

public class Test extends Two{
 	public static void main(String[] args)
 	{
 		A a;
 		Two t = new Two();
                
 		a = t;
 		a.method1();                
                //((Two) a).dormir();
 	}
 }