/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_1_3;

/**
 *
 * @author Usuario
 */

import java.io.*;


class Base{
    public void amethod()throws FileNotFoundException{}
}
public class ExcepDemo extends Base{
    public static void main(String argv[]){
        ExcepDemo e = new ExcepDemo();
    }
    public void amethod(){}
    
    ExcepDemo(){
        try{
            DataInputStream din = new DataInputStream(System.in);
            System.out.println("Pausing");
            din.readByte();
            System.out.println("Continuing");
            this.amethod();
            //
            if (true){
                throw new FileNotFoundException("");
            }            
        }catch(IOException ioe) {
            System.out.println(ioe.getMessage());
        } catch (Exception ex){
            System.out.println("Alguna exception");
        }
    }
}

