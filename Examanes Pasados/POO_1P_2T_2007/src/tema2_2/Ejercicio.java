/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema2_2;

/**
 *
 * @author Usuario
 */
public class Ejercicio {
    public static void main(String [] args){
        Test.main(args);
    }
}

class Rectangle{
    public int area(int length , int width) {
      return  length * width; 
    }
  }

  class Square extends Rectangle{
    public int area(float length, float width) {
      return  (int) Math.pow(length ,2);
    }
  }
  class Test{
    public static void main(String args[]) {
      Square r = new Square(); //
      System.out.println(r.area(5 , 4)); 
    }   
  }


