/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema2_1;

/**
 *
 * @author Usuario
 */
public class Ejercicio {
    public static void main(String[] args){
        Test.main(args);
    }
}

interface A{
 	public abstract void method1();
  }
class One implements A{
      public void method1(){
              System.out.println("hello");
      }
}
class Two extends One{}
class Test extends Two{
      public static void main(String[] args)
      {
              A a; // interfaz
              Two t = new Two(); //hereda un metodo method1() -> hello
              a = t;
              a.method1();
      }
 }