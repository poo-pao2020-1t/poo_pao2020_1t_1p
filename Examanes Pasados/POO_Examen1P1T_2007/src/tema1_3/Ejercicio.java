/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema1_3;

public class Ejercicio {
    public static void main (String[] args ){
        Test.main(args);
    }
}

abstract class A{
       public abstract void method1();
}
class One extends A{
       public void method1(){
               System.out.println("hello");
       }
}

class Two extends One{}

class Three extends A{
    public void method1() {
        System.out.println("hola #3");
    }
}


class Test extends Two{
      public static void main(String[] args)
      {
          
              A a;
              
              Two t = new Two();
              a = t;
              a.method1();
              
              /**
              Three x = new Three();
              a = x;
              a.method1();
              **/
      }
}

