/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema1_1;

/**
 *
 * @author Usuario
 */
public class Ejercicio {
    public static void main (String[] args){
        Prueba.main(args);
    }
}

class Mamifero{
    void comer(Mamifero m){
      System.out.println("Mamifero come comida");
    }
}
    class Ganado extends Mamifero {
        void comer(Ganado c){
          System.out.println("Ganado come heno");
        }
    }
        class Caballo extends Ganado{
            void comer(Caballo h){
              System.out.println("Caballo come heno");
            }
        }
class Prueba{
    public static void main(String[] args){
       Mamifero h = new Caballo();
       // h es un mamifero que tiene un metodo void comer(mamifero m)
       //y solamente podremos usar ese metodo y no el de caballo o ganado
       //si la firma fuera la misma entonces lo que obtendriamos seria su sobreescritura en caballo
       
       Ganado c = new Caballo();  
       // c es un ganado que tiene dos metodo void comer(mamifero m) y void comer(ganado c)
       //y solamente podremos usar esos y no el de caballo
       //si la firma fuera la misma entonces lo que obtendriamos seria su sobreescritura en caballo
       
       //aqui NO HAY SOBREESCRITURA porque los metodos RECIBEN DIFERENTES ARGUMENTOS
       
       
       c.comer(h);       
       //c tiene acceso al ser un ganado a comer(mamifero m) y comer(ganado c)
       // y como h esta como mamifero entonces por eso imprime que es un mamifero
       
       //llama al superior
       
       //hay que tener cuidado entre la sobrecarga y la sobreescritura porfavor estudiar
       
       
    }
}

