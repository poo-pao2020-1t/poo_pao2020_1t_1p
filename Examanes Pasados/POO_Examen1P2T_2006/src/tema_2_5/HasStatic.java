/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tema_2_5;

/**
 *
 * @author Usuario
 */

class HasStatic {
	private static int x = 100;
	public static void main(String ar[]) {
		HasStatic hs1 = new HasStatic();
		hs1.x++;
		HasStatic hs2 = new HasStatic();
		hs2.x++;
		hs1 = new HasStatic();
		hs1.x++;
		HasStatic.x++;
		System.out.println("x = " + x);
	}
 }

